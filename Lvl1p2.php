<?php
session_start();
$tabQuestion=[
  "Comment s'appelle le frère de Mario, le célèbre héros de Nintendo ? ",
  "De quel type d'animal est Sonic, le célèbre héros de Sega ?",
  "Si on vous dit : Franklin, Trevor, Michael, vous nous dites...",
  "Comment appelle t-on les Jeux de rôle en ligne massivement multijoueur ?",
  "Snake est un jeu qui a été intégré de base sur les téléphones ..."];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\quiz.css">
  <title>Document</title>
</head>
<body>
 <div class="parent">

<!-- HEADER -->

    <div class="div1">
<!-- LOGO  -->
            <img class="logoNav"src="images\logosims.png" alt="">
<!-- TITRE HEADER -->
            <h1 class="titreNav">QUIZZ CULTURE JEUX VIDEOS</h1>
<!-- BOUTON RESTART-->
             <a href="index.php" class="restartButton"><b>RESTART</b></a>

    </div>

<!-- TITRE DU LEVEL -->

    <div class="div2">
        <h2 class="titreLevel">Level 1</h2>
        </div>

<!-- C'EST LE TABLEAU -->

    <div class="div3">
        <div class="div3_1">
            <img class="bgTableau"src="images\BG_lvl1.png" alt="image d'arrière plan de streetfighter">

            <h2 class="alertPhone"><img class="imgAlertPhone" src="images\alertePhone.png" alt=""><br><br>Veuillez mettre votre téléphone au format horizontale pour profiter au mieux du quiz.</h2>
        </div>
      <img class="barreLifeR"src="images\barre_life_<?php echo "".$_SESSION['scoreA']."" ?>.png" alt="">
      <img class="barreLifeL"src="images\barre_life_<?php echo "".$_SESSION['scoreE']."" ?>.png" alt="">
      <img class="perso1" src="images\perso1_lvl1.gif" alt="ryu">
      <img class="perso2" src="images\perso2_lvl1.gif" alt="ken">

    </div>

<!-- QUESTIONS -->

    <div class="div4">
      <?php echo "<p class=\"question\">Question ".$_SESSION['x']." : ".$tabQuestion[$_SESSION['x']-1]."</p>";
       ?>
    </div>

<!-- BOUTONS REPONSES -->

        <?php

          echo "<div class=\"div5\">

          ".$_SESSION['reponse']."

        </div>";

        ?>

    <!-- BOUTON VALIDATION -->

        <div class="div6">

              <?php
                  if ($_SESSION['scoreA']==3) {

                    echo "<form action=\"2intermediairelvl1.php\" method=\"post\">
                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"Niveau suivant\">

                  </form>";

                  }elseif($_SESSION['scoreE']==3) {

                    echo "<form action=\"index.php\" method=\"post\">

                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"RETRY\">";

                  }else{
                    echo"<form action=\"2intermediairelvl1.php\" method=\"post\">

                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"Question suivante\">

                  </form>";
                  }

                ?>



        </div>

</div>
</body>
<script src="js\quiz.js" defer></script>
</html>
